var banner = "\n"+
" _______          _     _     _ ______              _       \n"+
"(_______)        | |   (_)   (_|____  \\            | |      \n"+
"    _ _____  ____| |__  _     _ ____)  )_____ ____ | |  _   \n"+
"   | | ___ |/ ___)  _ \\| |   | |  __  ((____ |  _ \\| |_/ )  \n"+
"   | | ____( (___| | | | |___| | |__)  ) ___ | | | |  _ (   \n"+
"   |_|_____)\\____)_| |_|\\_____/|______/\\_____|_| |_|_| \\_)  \n";

console.log(banner);

var express = require('express'),  
  http = require('http'),
  https = require('https'),
  fs = require('fs')
  bodyParser = require('body-parser'),
  rj = require('request-json'),
  mongo = require('mongodb');
  

var app = express(),
  httpPort = process.env.PORT || 7000,
  httpsPort = process.env.PORT || 7443,
  privateKey  = fs.readFileSync('keys/express.key', 'utf8'),
  certificate = fs.readFileSync('keys/express.crt', 'utf8');

app.use(bodyParser.json({limit: "1mb"}));
  
var credentials = {key: privateKey, cert: certificate};
var httpsServer = https.createServer(credentials, app);
var httpServer = http.createServer(app);


app.all('*', function(req, res, next){
  if(req.secure){
    return next();
  };
  res.redirect('https://' + req.hostname + ":" + httpsPort + req.url);
});

var routes = require('./api/routes/apiRoutes'); //importing route
routes(app); //register the route

httpsServer.listen(httpsPort);
httpServer.listen(httpPort);

console.log('TechU Bank RESTful API server started on (HTTP): ' + httpPort);
console.log('TechU Bank RESTful API server started on (HTTPS): ' + httpsPort);
