'use strict';
module.exports = function(app) {
  var gt = require('../controllers/grantingTicketController'),
      mov = require('../controllers/movementsController'),
      prod = require('../controllers/productsController'),
      acc = require('../controllers/accountsController'),
      us = require('../controllers/usersController'),
      cat = require('../controllers/categoriesController'),
      proms = require('../controllers/promotionsController'),
      trans = require('../controllers/transfersController'),
      wt = require('../controllers/weatherController');

  // todoList Routes
  app.route('/api/v1/grantingTicket')
    .post(gt.create_gt)
    .put(gt.verify_ticket, gt.update_gt);

  app.route('/api/v1/users')
    .post(us.create_user);

  app.route('/api/v1/weather')
    .get(wt.get_weather);
  
  app.route('/api/v1/categories')
    .get(gt.verify_ticket, cat.get_categories);

  app.route('/api/v1/users/:user/accounts')
    .get(gt.verify_ticket, acc.get_user_accounts);

  app.route('/api/v1/users/:user/accounts/:account')
    .put(gt.verify_ticket, acc.modify_user_account);

  app.route('/api/v1/users/:user/accounts/:account/movements')
    .get(gt.verify_ticket, mov.get_account_movements);

  app.route('/api/v1/users/:user/accounts/:account/transfers')
    .post(gt.verify_ticket, trans.create_transfer);

  app.route('/api/v1/users/:user/promotions')
    .get(gt.verify_ticket, proms.get_promotions);

  app.use('/api/v1/admin/*', gt.verify_admin_ticket);

  app.route('/api/v1/admin/users')
    .get(us.get_users)
    .post(us.create_user);

  app.route('/api/v1/admin/users/:user')
    .get(us.get_users)
    .put(us.modify_user)
    .delete(us.delete_user);

  app.route('/api/v1/admin/accounts')
    .post(acc.create_account);

  app.route('/api/v1/admin/accounts/:account')
    .put(acc.modify_account)
    .delete(acc.delete_account);

  app.route('/api/v1/admin/products')
    .get(prod.get_products)
    .post(prod.create_product);

  app.route('/api/v1/admin/products/:product')
    .get(prod.get_products)
    .put(prod.modify_product)
    .delete(prod.delete_product);

  app.route('/api/v1/admin/movements')
    .get(mov.get_movements)
    .post(mov.create_movement);

  app.route('/api/v1/admin/movements/:movement')
    .get(mov.get_movements)
    .put(mov.modify_movement)
    .delete(mov.delete_movement);

  app.route('/api/v1/admin/categories')
    .get(cat.get_categories)
    .post(cat.create_category);

  app.route('/api/v1/admin/categories/:category')
    .get(cat.get_categories)
    .put(cat.modify_category)
    .delete(cat.delete_category);

  app.route('/api/v1/admin/promotions')
    .get(proms.get_promotions)
    .post(proms.create_promotion);

  app.route('/api/v1/admin/promotions/:promotion')
    .get(proms.get_promotions)
    .put(proms.modify_promotion)
    .delete(proms.delete_promotion);

};
