// config.js
module.exports = {
  TOKEN_SECRET: process.env.TOKEN_SECRET || "tokenultrasecretodelamuertesiempreiwillsurvive",
  DATABASE_URL: "postgres://techu:" + process.env.PG_PWD + "@postgres:5432/techu",
  MONGO_URL: "mongodb://techu:" + process.env.MONGO_PWD + "@mongo:27017/techU",
  PRODUCT_BASE_URL: "https://s3-eu-west-1.amazonaws.com/usr6-demobucket/resources/",
  PAGE_SIZE: 20,
  MLAB_URL: "https://api.mlab.com/api/1/databases/bdbanca/collections/movimientos",
  MLAB_API_KEY: "apiKey=3X3zT-GCyKOJGmIwJ5jPibhovgMIPDe-",
  WEATHER_URL: "https://api.openweathermap.org/data/2.5/weather",
  WEATHER_API_KEY: "5da311b125cefeb68ba26f096b5595fa",
  WEATHER_ICON_BASE_URL: "https://openweathermap.org/img/w/",
  WEATHER_ICON_EXT: ".png",
  JWT_EXP_PERIOD: 300,
};