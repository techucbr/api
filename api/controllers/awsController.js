'use strict'
var AWS = require('aws-sdk');

AWS.config.loadFromPath('./api/config.json');

exports.uploadImageToBucket = function(file, name, callback){
  var s3 = new AWS.S3({apiVersion: '2006-03-01'});
  var params = {
    ACL: "public-read",
    Body: file,
    Bucket: "usr6-demobucket",
    Key: "resources/" + name,
    StorageClass: "STANDARD"
  }
  s3.putObject(params, function(err, data){
    if(err){
      return callback({status: 500, success: false, error: err});
    }
    return callback({status: 200, success: true, data: data});
  });
}

exports.deleteImageFromBucket = function(name, callback){
  var s3 = new AWS.S3({apiVersion: '2006-03-01'});
  var params = {
    Bucket: "usr6-demobucket",
    Key: "resources/" + name
  }
  s3.deleteObject(params, function(err, data){
    if(err){
      return callback({status: 500, success: false, error: err});
    }
    return callback({status: 200, success: true, data: data});
  });
}
