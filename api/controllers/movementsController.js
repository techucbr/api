'use strict';
var config = require('../config');
const { Pool, Client } = require('pg');
const rj = require('request-json');

exports.get_account_movements = function(req, res) {
  var query = {};
  query.user = {"$eq": req.params.user};
  query.account = {"$eq": req.params.account};
  
  if(req.query.con){
    query.$or = [{}, {}];
    query.$or[0].concept = {"$regex": req.query.con, "$options": "i"}
    query.$or[1].detail = {"$regex": req.query.con, "$options": "i"}
  }
  if(req.query.min){
    query.quantity = {};
    query.quantity.$gte = new Number(req.query.min);
  }
  if(req.query.max){
    if(!query.quantity){
      query.quantity = {};
    }
    query.quantity.$lte = new Number(req.query.max);
  }
  if(req.query.from){
    query.date = {}
    query.date.$gte = {};
    query.date.$gte.$date = new Date(req.query.from);
  }
  if(req.query.to){
    query.date = {}
    query.date.$lte = {};
    query.date.$lte.$date = new Date(req.query.to);
  }
  if(req.query.category){
    query.category = req.query.category;
  }
  if(req.query.type){
    if(req.query.type == 0){
      query.type = new Number(req.query.type);
    }
    else if(req.query.type == 1){
      query.type = {};
      query.type.$gte = new Number(req.query.type);
    }
  }
  
  var queryString = "&q=" + JSON.stringify(query);

  var client = rj.createClient(config.MLAB_URL + "?" + config.MLAB_API_KEY + queryString + "&c=true");
  client.get("", function(err, r, body){
    if(err){
      res.status(err.status).send(body);
    }
    else{
      var count = body;
      var pageSize = config.PAGE_SIZE;
      if(req.query.page_size){
        pageSize = req.query.page_size;
      }
      queryString += "&l=" + pageSize;
      if(req.query.page){
        queryString += "&sk=" + (pageSize * req.query.page);
      }
      queryString += "&s={\"date\": -1, \"quantity\": -1}";
      var client = rj.createClient(config.MLAB_URL + "?" + config.MLAB_API_KEY + queryString);
      client.get("", function(err, r, body){
        if(err){
          res.status(err.status).send(body);
        }
        else{
          for(var mov in body){
            body[mov]["id"] = body[mov]._id.$oid;
            var date = new Date(body[mov].date.$date); 
            
            body[mov]["dateFormat"] = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' +  date.getDate();
          }
          res.send({totalCount: count, count: body.length, page: parseInt(req.query.page) || 0, pageSize: pageSize, movements: body});
        }
      });
    }
  });
};

exports.get_movements = function(req, res) {
  var queryString = "&c=true"
  if(req.params.movement){
    queryString += "&q={\"id\":\"" + req.params.movement+ "\"}"
  }
  var client = rj.createClient(config.MLAB_URL + "?" + config.MLAB_API_KEY + queryString);
  client.get("", function(err, r, body){
    if(err){
      res.status(err.status).send(body);
    }
    else{
      var count = body;
      var pageSize = config.PAGE_SIZE;
      if(req.query.page_size){
        pageSize = req.query.page_size;
      }
      queryString = "&l=" + pageSize;
      if(req.query.page){
        queryString += "&sk=" + (config.PAGE_SIZE * req.query.page);
      }
      if(req.params.movement){
        queryString += "&q={\"id\":\"" + req.params.movement+ "\"}"
      }
      var client = rj.createClient(config.MLAB_URL + "?" + config.MLAB_API_KEY + queryString);
      client.get("", function(err, r, body){
        if(err){
          res.status(err.status).send(body);
        }
        else{
          res.send({totalCount: count, count: body.length, page: req.query.page || 0, movements: body});
        }
      });
    }
  });
};

exports.create_movement = function(req, res) {
  checkMovement(req, function(valid){
    if(!valid.success){
      return res.status(valid.status).send({error: valid.error, code: valid.code});
    }
    var merchantAddress = undefined;
    if(req.body.address){
      merchantAddress = req.body.address + ", " + req.body.city + ", " + req.body.ZIP;
    }

    getGoogleLocation(merchantAddress, function(location){
      var date = new Date(req.body.dateFormat);
      var data = {
        user: req.body.user,
        date: {$date: date.toISOString()},
        type: req.body.type,
        concept: req.body.concept,
        detail: req.body.detail,
        category: req.body.category,
        quantity: new Number(req.body.quantity),
        merchant: req.body.merchant,
        address: req.body.address,
        ZIP: req.body.ZIP,
        city: req.body.city,
        account: req.body.account,
        location: location,
      };
      var client = rj.createClient(config.MLAB_URL + "?" + config.MLAB_API_KEY);
      client.post("", data, function(err, r, body){
        if(err){
          res.status(err.status).send(body);
        }
        else{
          res.status(201).send(body);
        }
      });
    });
  });
};

exports.modify_movement = function(req, res) {
  checkMovement(req, function(valid){
    if(!valid.success){
      return res.status(valid.status).send({error: valid.error, code: valid.code});
    }
    var data = {"$set": {}}
    data.$set.user = req.body.user;
    var date = new Date(req.body.dateFormat);
    data.$set.date = {"$date": date.toISOString()};
    data.$set.type = req.body.type;
    data.$set.concept = req.body.concept;
    data.$set.detail = req.body.detail;
    data.$set.category = req.body.category;
    data.$set.quantity = req.body.quantity;
    data.$set.merchant = req.body.merchant;
    data.$set.address = req.body.address;
    data.$set.ZIP = req.body.ZIP;
    data.$set.city = req.body.city;
    data.$set.account = req.body.account;
    var newAddress = req.body.address + ", " + req.body.ZIP + ", " + req.body.city;
    getGoogleLocation(newAddress, function(location){
      data.$set.location = location;
      sendUpdate(req, res, data);  
    });
  });
};

exports.delete_movement = function(req, res) {
  var client = rj.createClient(config.MLAB_URL + "/" + req.params.movement + "?" + config.MLAB_API_KEY);
  client.delete("", function(err, r, body){
    if(err){
      res.status(err.status).send(body);
    }
    else{
      res.status(204).send();
    }
  });
};

function sendUpdate(req, res, data){
  var client = rj.createClient(config.MLAB_URL + "/" + req.params.movement + "?" + config.MLAB_API_KEY);
  client.put("", data, function(err, r, body){
    if(err){
      res.status(err.status).send(body);
    }
    else{
      res.status(200).send(body);
    }
  });
}

function checkMovement(req, callback){
  if(req.body.address || req.body.ZIP || req.body.city){
    if(!req.body.address || !req.body.ZIP || !req.body.city){
      return callback({success: false, status: 400, error: "Dirección, ZIP y ciudad deben aparecer a la vez", code: 1});
    }
  }
  
  if(req.body.type > 0){
    if(!req.body.address && !req.body.ZIP && !req.body.city){
      return callback({success: false, status: 400, error: "El movimiento debe contener la dirección del comercio y el comercio", code: 2});
    }
    if(!req.body.merchant){
      return callback({success: false, status: 400, error: "El movimiento debe contener la dirección del comercio y el comercio", code: 3});
    }
  }

  if(!req.body.user){
    return callback({success: false, status: 400, error: "El movimiento debe contener el usuario", code: 4});
  }
  if(!req.body.dateFormat){
    return callback({success: false, status: 400, error: "El movimiento debe contener fecha", code: 5});
  }
  if(!req.body.hasOwnProperty("type")){
    return callback({success: false, status: 400, error: "El movimiento debe contener tipo", code: 6});
  }
  if(!req.body.concept){
    return callback({success: false, status: 400, error: "El movimiento debe contener concepto", code: 7});
  }
  if(!req.body.detail){
    return callback({success: false, status: 400, error: "El movimiento debe contener detalle", code: 8});
  }
  if(!req.body.account){
    return callback({success: false, status: 400, error: "El movimiento debe pertenecer a una cuenta", code: 9});
  }
  if(!req.body.hasOwnProperty("quantity")){
    return callback({success: false, status: 400, error: "El movimiento debe tener cantidad", code: 10});
  }

  const client = new Client({
    connectionString: config.DATABASE_URL,
  })
  client.connect().then(function(){
    var query = client.query('SELECT id FROM user_accounts WHERE user_id = $1 AND id = $2', [req.body.user, req.body.account]);
    query.then(function(q){
      if(q.rowCount != 1){
        callback({success: false, status: 400, error: "Usuario y cuenta no están relacionados", code: 11});
      }
      else{
        return callback({status: 200, success: true});
      }
      client.end();
    }).catch(function(e){
      console.log(e);
      callback({success: false, status: 400, error: "Usuario y producto no están relacionados", code: 11});
      client.end();
    })
  }).catch(function(e){
    console.log(e);
    callback({success: false, status: 400, error: "Usuario y producto no están relacionados", code: 11});
  });
}

function getGoogleLocation(address, callback, promise){
  if(!address){
    return callback();
  }
  var googleMapsClient = require('@google/maps').createClient({
    key: 'AIzaSyDT0niPcpR6g1fmW_txbM5VrIX0AuCt5Yc'
  });
  googleMapsClient.geocode({
    address: address
  }, function(err, response) {
    if (!err) {
      var location = {};
      location.lat = response.json.results[0].geometry.location.lat;
      location.lng = response.json.results[0].geometry.location.lng;
      callback(location);
    }
  });
}