'use strict';
var config = require('../config');
var aws = require('./awsController');
const { Pool, Client } = require('pg');
exports.get_products = function(req, res) {
  const client = new Client({
    connectionString: config.DATABASE_URL,
  })
  client.connect().then(function(){
    var filter = "";
    var parameters = [];
    if(req.params.product){
      filter = 'WHERE id = $1 ';
      parameters.push(req.params.product);
    }
    var query = client.query('SELECT id, name, description, file FROM products ' + filter + ' ORDER BY id', parameters);
    query.then(function(q){
      var products = [];
      for(var i = 0; i < q.rowCount; i++){
        products[i] = {id: q.rows[i].id,
                       name: q.rows[i].name,
                       description: q.rows[i].description,
                       fileName: q.rows[i].file,
                       url: config.PRODUCT_BASE_URL + q.rows[i].file};
      }
      
      res.status(200).send({count: products.length, products: products});
      client.end();
    }).catch(function(e){
      console.log(e);
      res.status(500).send({message: "Error con la base de datos", success: false, data: e.error});
      client.end();
    });
  }).catch(function(e){
    console.log(e);
    res.status(500).send({message: "Error con la base de datos", success: false, data: e.error});
  });
};

exports.create_product = function(req, res) {
  const client = new Client({
    connectionString: config.DATABASE_URL,
  })
  if(!req.body.name){
    return res.status(500).send({message: "Debe proporcionar el nombre"});
  }
  if(!req.body.description){
    return res.status(500).send({message: "Debe proporcionar una descripción"});
  }
  if(!req.body.image || !req.body.newFileName){
    return res.status(500).send({message: "Debe proporcionar una imagen"});
  }
  client.connect().then(function(){
    var query = client.query('SELECT name FROM products WHERE name = $1', [req.body.name]);

    query.then(function(q){
      if(q.rowCount > 0){
        return res.status(500).send({message: "Producto ya existente"});
      }

      var image = new Buffer(req.body.image.split('base64,')[1], "base64");
      aws.uploadImageToBucket(image, req.body.newFileName, function(awsRes){
        if(awsRes.success){
          query = client.query('INSERT INTO products(name, description, file) VALUES ($1, $2, $3) RETURNING id', 
                              [req.body.name, req.body.description, req.body.newFileName]);

          query.then(function(q){
            if(q.rowCount != 1){
              res.status(500).send({message: "Error con la base de datos", success: false, data: "Error al crear el usuario"});
            }
            else{
              var product = {id: q.rows[0].id,
                            name: req.body.name,
                            description: req.body.description,
                            fileName: req.body.newFileName,
                            url: config.PRODUCT_BASE_URL + req.body.newFileName};
              res.status(201).send({message: "Producto creado!", product: product});
            }
            client.end();            
          }).catch(function(e){
            console.log(e);
            res.status(500).send({message: "Error con la base de datos", success: false, data: e.error});
            client.end();
          });
        }
        else{
          res.status(500).send({message: "Error al subir la imagen", success: false, data: awsRes.error});
          client.end();
        }      
      });
    }).catch(function(e){
      console.log(e);
      res.status(500).send({message: "Error con la base de datos", success: false, data: e.error});
      client.end();
    });
  }).catch(function(e){
    console.log(e);
    return res.status(500).send({message: "Error con la base de datos", success: false, data: e.error});
  });
};

exports.modify_product = function(req, res) {
  const client = new Client({
    connectionString: config.DATABASE_URL,
  })
  client.connect().then(function(){
    var i = 1;
    var fields = "";
    var params = [];
    if(req.body.name){
      fields = "name = $" + i++;
      params.push(req.body.name);
    }
    if(req.body.description){
      if(i > 1){
        fields += ", ";
      }
      fields += "description = $" + i++;
      params.push(req.body.description);
    }
    if(req.body.newFileName){
      if(!req.body.image){
        return res.status(500).send({message: "Debe proporcionar una imagen", success: false, data: e.error});
      }
      if(i > 1){
        fields += ", ";
      }
      fields += "file = $" + i++;
      params.push(req.body.newFileName);

      var image = new Buffer(req.body.image.split('base64,')[1], "base64");
      aws.uploadImageToBucket(image, req.body.newFileName, function(awsRes){
        if(awsRes.success){
          req.body.fileName = req.body.newFileName;
          return exports.update_product_database(params, fields, req, res, client, i);
        }
        else{
          return res.status(500).send({message: "Error al subir imagen", success: false, data: e.error});
        }
      });
    }
    else{
      return exports.update_product_database(params, fields, req, res, client, i);
    }
  }).catch(function(e){
    console.log(e);
    return res.status(500).send({message: "Error con la base de datos", success: false, data: e.error});
  });
};

exports.update_product_database = function(params, fields, req, res, client, i){
  if(params.length == 0){
    return res.status(500).send({message: "No se han indicado campos!", success: false, });  
  }
  params.push(req.params.product);
  var query = client.query('UPDATE products SET ' + fields + ' WHERE id = $' + i, params);
  query.then(function(q){
    if(q.rowCount != 1){
      res.status(500).send({message: "Error con la base de datos", success: false, data: "Error al actualizar el producto"});
    }
    else{
      var product = {id: req.params.product,
                    name: req.body.name,
                    description: req.body.description,
                    fileName: req.body.fileName,
                    url: config.PRODUCT_BASE_URL + req.body.fileName};
      res.status(200).send({message: "Producto modificado!", product: product});          
    }
    client.end();
  }).catch(function(e){
    console.log(e);
    res.status(500).send({message: "Error con la base de datos", success: false, data: e.error});
    client.end();
  });
}

exports.delete_product = function(req, res) {
  const client = new Client({
    connectionString: config.DATABASE_URL,
  })
  client.connect().then(function(){
    var query = client.query('DELETE FROM user_accounts WHERE product_id = $1', [req.params.product]);
    query.then(function(q){
      query = client.query('DELETE FROM products WHERE id = $1 RETURNING file', [req.params.product]);
      query.then(function(q){
        if(q.rowCount != 1){
          res.status(500).send({message: "Error con la base de datos", success: false, data: "Error al borrar el producto"});
          client.end();
        }
        else{
          res.status(201).send();
          client.end();
        }
      }).catch(function(e){
        console.log(e);
        res.status(500).send({message: "Error con la base de datos", success: false, data: e.error});
        client.end();        
      });
    }).catch(function(e){
      console.log(e);
      res.status(500).send({message: "Error con la base de datos", success: false, data: e.error});
      client.end();        
    });
  }).catch(function(e){
    console.log(e);
    res.status(500).send({message: "Error con la base de datos", success: false, data: e.error});
  });
};