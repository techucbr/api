'use strict';
var jwt = require('jwt-simple');
var config = require('../config');
var moment = require('moment');
var users = require('./usersController');
var accs = require('./accountsController');

var check_ticket = function(req, res){
  if(!req.headers.authorization) {
    return {success: false, error: "Request without Authorization header"};
  }
  
  var token = req.headers.authorization.split(" ")[1];
  try{
    var payload = jwt.decode(token, config.TOKEN_SECRET);
  }
  catch(err){
    return {success: false, error: "El Token no es valido!"};
  }

  if(payload.exp <= moment().valueOf()) {
    return {success: false, error: "El Token ha expirado!"};
  }

  if(!payload.adm && req.params.user && req.params.user != payload.sub){
    return {success: false, error: "No tiene permisos para acceder al usuario solicitado"};
  }

  if(!payload.adm && req.params.account){
    var found = false;
    for(var i in payload.acs){
      if(payload.acs[i] == req.params.account){
        found = true;
        break;
      }
    }
    if(!found){
      return {success: false, error: "No tiene permisos para acceder a la cuenta solicitada"};
    }
  }
  req.payload = payload;
  return {success: true, admin: payload.adm};
}

exports.create_gt = function(req, res) {
  users.verify_user(req.body.user, req.body.password, function(userInfo){    
    if(!userInfo.success){
      return res.status(403).send({"error": userInfo.error});
    }

    accs.query_user_accounts(userInfo.user.id, function(response){
      if(response.success){
        var expires = moment().add(config.JWT_EXP_PERIOD, "seconds").valueOf();
        
        var tokenPayload = {
            sub: userInfo.user.id,
            iat: moment().valueOf(),
            exp: expires,
            adm: userInfo.user.admin,
            usr: userInfo.user.username,
            acs: [] 
          };

        for(var i in response.accounts){
          tokenPayload.acs[i] = response.accounts[i].id;
        }
        var token = jwt.encode(tokenPayload, config.TOKEN_SECRET);
    
        var payload = tokenPayload;
        payload.name = userInfo.user.name;
        payload.sn = userInfo.user.surname;
    
        res.status(201).send({"ticket": token, "payload": payload});
      }
      else{
        res.status(response.status).send({message: response.message, success: false, data: response.data});
      }
    });
  });
};

exports.verify_ticket = function(req, res, next){
  var result = check_ticket(req, res);
  if(!result.success){
    return res.status(401).send({message: result.error});
  }
  next();
}

exports.verify_admin_ticket = function(req, res, next){
  var result = check_ticket(req, res);
  
  if(!result.success){
    return res.status(401).send({message: result.error});
  }

  if(!result.admin){
    return res.status(403).send({message: "Tienes que ser admin para ejecutar esta oepración!"});
  }
  
  next();
}

exports.update_gt = function(req, res) {
  var expires = moment().add(config.JWT_EXP_PERIOD, "seconds").valueOf();
  var tokenPayload = {
    sub: req.payload.sub,
    iat: moment().valueOf(),
    exp: expires,
    adm: req.payload.adm,
    usr: req.payload.usr,
    acs: req.payload.acs,
  };

  var token = jwt.encode(tokenPayload, config.TOKEN_SECRET);

  res.status(201).send({"ticket": token, "payload": tokenPayload});
};
