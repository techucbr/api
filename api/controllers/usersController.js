'use strict';
const { Pool, Client } = require('pg');
var config = require('../config');
var accs = require('./accountsController');
var passwordHash = require('password-hash');

exports.create_user = function(req, res) {
  if(!req.body.user){
    return res.status(400).send({message: "Debe introducir un nombre de usuario válido"});
  }
  if(!req.body.password){
    return res.status(400).send({message: "Debe introducir una contraseña"});
  }
  if(!req.body.name){
    return res.status(400).send({message: "Debe introducir un nombre"});
  }
  if(!req.body.surname){
    return res.status(400).send({message: "Debe introducir un apellido"});
  }

  if(req.body.password != req.body.passwordR){
    return res.status(400).send({message: "Las contraseñas no coinciden!"});
  }

  const client = new Client({
    connectionString: config.DATABASE_URL,
  })
  client.connect().then(function(){
    var query = client.query('SELECT id FROM users WHERE username = $1', [req.body.user]);

    query.then(function(q){
      if(q.rowCount > 0){
        return res.status(500).send({message: "Usuario ya existente"});
      }

      var encPassword = passwordHash.generate(req.body.password);

      query = client.query('INSERT INTO users(username, name, surname, password, admin) VALUES ($1, $2, $3, $4, $5) RETURNING id', 
        [req.body.user, req.body.name, req.body.surname, encPassword, req.body.admin]);
      
        query.then(function(q){
          if(q.rowCount != 1){
            client.end();
            return res.status(500).send({message: "Error con la base de datos", success: false, data: "Error al crear el usuario"});
          }
          req.body.username = req.body.user;
          req.body.user_id = q.rows[0].id;
          client.end();
          return res.status(201).send({message: "Usuario creado!", user: exports.mapUser(req.body, [])});
        }).catch(function(e){
          console.log(e);
          client.end();
          return res.status(500).send({message: "Error con la base de datos", success: false, data: e.error});
        });
    }).catch(function(e){
      console.log(e);
      client.end();
      return res.status(500).send({message: "Error con la base de datos", success: false, data: e.error});
    });
  }).catch(function(e){
    console.log(e);
    return res.status(500).send({message: "Error con la base de datos", success: false, data: e.error});
  });
};

exports.get_users = function(req, res){
  const client = new Client({
    connectionString: config.DATABASE_URL,
  })
  client.connect().then(function(){
    var filter = "";
    var parameters = [];
    if(req.params.user){
      filter = 'WHERE  users.id = $1 ';
      parameters.push(req.params.user);
    }
    var query = client.query('SELECT u.id as user_id, username, u.name as name, surname, admin, p.id as product_id, a.id as account_id, code, balance, alias, p.name as product_name, description, file as fileName ' + 
      'FROM users u LEFT JOIN user_accounts a ON u.id = a.user_id LEFT JOIN products p ON p.id = a.product_id ' + 
      filter +
      'ORDER BY user_id ASC, product_id ASC, code ASC', parameters);

    query.then(function(q){
      var users = [];
      var currentUser = {};
      var accounts = [];
      for(var i in q.rows){
        var row = q.rows[i];
        if(currentUser.id !== row.user_id){
          accounts = [];
          currentUser = exports.mapUser(row, accounts);
          users.push(currentUser);
        }
        var lastAccount = accs.mapAccount(row);
        if(lastAccount){
          accounts.push(lastAccount);
        }
      }
      client.end();
      return res.status(200).send({count: users.length, users: users});
    }).catch(function(e){
      console.log(e);
      client.end();
      return res.status(500).send({message: "Error con la base de datos", success: false, data: e.error});
    });
  }).catch(function(e){
    console.log(e);
    return res.status(500).send({message: "Error con la base de datos", success: false, data: e.error});
  });
};

exports.modify_user = function(req, res){
  const client = new Client({
    connectionString: config.DATABASE_URL,
  })
  client.connect().then(function(){
    var i = 1;
    var fields = "";
    var params = [];
    if(req.body.user){
      fields = "username = $" + i++;
      params.push(req.body.user);
    }
    if(req.body.password){
      if(req.body.password != req.body.passwordR){
        client.end();
        return res.status(401).send({message: "Las contraseñas no coinciden!"});
      }
      if(i > 1){
        fields += ", ";
      }
      fields += "password = $" + i++;
      params.push(passwordHash.generate(req.body.password));
    }
    if(req.body.name){
      if(i > 1){
        fields += ", ";
      }
      fields += "name = $" + i++;
      params.push(req.body.name);
    }
    if(req.body.surname){
      if(i > 1){
        fields += ", ";
      }
      fields += "surname = $" + i++;
      params.push(req.body.surname);
    }
    if(req.body.admin){
      if(i > 1){
        fields += ", ";
      }
      fields += "admin = $" + i++;
      params.push(req.body.admin);
    }

    if(params.length == 0){
      client.end();
      return res.status(500).send({message: "No se han indicado campos!", success: false, });  
    }
    params.push(req.params.user);
    var query = client.query('UPDATE users SET ' + fields + ' WHERE id = $' + i, params);
    query.then(function(q){
      if(q.rowCount != 1){
        client.end();
        return res.status(500).send({message: "Error con la base de datos", success: false, data: "Error al actualziar el usuario"});
      }
      req.body.username = req.body.user;
      req.body.user_id = req.params.user;
      client.end();
      return res.status(200).send({message: "Usuario modificado!", user: exports.mapUser(req.body, req.body.accounts)})
    }).catch(function(e){
      console.log(e);
      client.end();
      return res.status(500).send({message: "Error con la base de datos", success: false, data: e.error});
    });
  }).catch(function(e){
    console.log(e);
    return res.status(500).send({message: "Error con la base de datos", success: false, data: e.error});
  });
};

exports.delete_user = function(req, res){
  const client = new Client({
    connectionString: config.DATABASE_URL,
  })
  client.connect().then(function(){
    var query = client.query('DELETE FROM user_accounts WHERE user_id = $1', [req.params.user]);
    query.then(function(q){
      var query = client.query('DELETE FROM users WHERE id = $1', [req.params.user]);
      query.then(function(q){
        if(q.rowCount != 1){
          client.end();
          return res.status(500).send({message: "Error con la base de datos", success: false, data: "Error al borrar el usuario"});
        }
        client.end();
        return res.status(204).send();
      }).catch(function(e){
        console.log(e);
        client.end();
        return res.status(500).send({message: "Error con la base de datos", success: false, data: e.error});
      })
    }).catch(function(e){
      console.log(e);
      client.end();
      return res.status(500).send({message: "Error con la base de datos", success: false, data: e.error});
    })
  }).catch(function(e){
    console.log(e);
    return res.status(500).send({message: "Error con la base de datos", success: false, data: e.error});
  });
};

exports.verify_user = function(user, password, callback){
  const client = new Client({
    connectionString: config.DATABASE_URL,
  })
  client.connect().then(function(){
    var query = client.query('SELECT id, username, password, name, surname, admin FROM users WHERE username = $1', [user]);

    query.then(function(res){
      if(res.rowCount != 1){
        callback({error: "Usuario y/o password incorrecto", success: false, detail: "Usuario inexistente"});
        client.end();
        return;
      }
      
      if(!passwordHash.verify(password, res.rows[0].password)){
        callback({error: "Usuario y/o password incorrecto", success: false, detail: "Password no válida"});
        client.end();
        return;
      }
      
      callback({user: res.rows[0], success: true});
      client.end();
      return;
    }).catch(function(e){
      console.log(e);
      callback({error: "Error con la base de datos", success: false, detail: e.error});
      client.end();
      return;
    });
  }).catch(function(e){
    console.log(e);
    callback({error: "Error con la base de datos", success: false, detail: e.error});
    return;
  });
};

exports.mapUser = function(row, accounts){
  var user = {id: row.user_id,
    name: row.name,
    surname:  row.surname,
    user: row.username,
    admin: row.admin,
    accounts: accounts};

  return user;
};
