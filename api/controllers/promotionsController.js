'use strict';
var config = require('../config');
var aws = require('./awsController');
var mongo = require('mongodb');
const MongoClient = require('mongodb').MongoClient;
var db;
MongoClient.connect(config.MONGO_URL, function(err, database) {
  if (err) return console.log(err)
  db = database;
})


exports.get_promotions = function(req, res) {
  var query = {};
  if(req.params.promotion){
    query._id = new mongo.ObjectID(req.params.promotion);
  }
  db.collection('promotions').find(query).toArray(function(err, promotions){
    if (err){
      console.log(err);
      return res.status(500).send({message: "Error al acceder a la base de datos", error: err});
    }
    for(var i in promotions){
      promotions[i].id = promotions[i]._id;
      promotions[i].url = config.PRODUCT_BASE_URL + promotions[i].fileName;
    }
    res.status(200).send({promotions: promotions})
  });
};

exports.create_promotion = function(req, res) {
  if(!req.body.name){
    return res.status(500).send({error: "La promoción debe contener nombre", code: 1});
  }
  if(!req.body.description){
    return res.status(500).send({error: "La promoción debe contener descripción", code: 2});
  }
  if(!req.body.merchant){
    return res.status(500).send({error: "La promoción debe contener comercio", code: 3});
  }
  if(!req.body.discount){
    return res.status(500).send({error: "La promoción debe contener descuento", code: 4});
  }
  if(!req.body.newFileName || !req.body.image){
    return res.status(500).send({error: "La promoción debe contener una imagen asociada", code: 5});
  }
  var promotion = {name: req.body.name,
                   description: req.body.description,
                   merchant: req.body.merchant,
                   discount: req.body.discount,
                   fileName: req.body.newFileName}
  db.collection('promotions').insert(promotion, function(err, promotion){
    if (err){
      console.log(err);
      return res.status(500).send({message: "Error al acceder a la base de datos", error: err});
    }
    var image = new Buffer(req.body.image.split('base64,')[1], "base64");
    aws.uploadImageToBucket(image, req.body.newFileName, function(awsRes){
      if(awsRes.success){
        promotion.ops[0].id = promotion.ops[0]._id;
        promotion.ops[0].url = config.PRODUCT_BASE_URL + req.body.newFileName;
        return res.status(201).send({promotion: promotion.ops[0]})
      }
      else{
        return res.status(500).send({message: "Error al subir imagen", success: false, data: e.error});
      }
    });
  });
};

exports.modify_promotion = function(req, res) {
  var data = {$set:{}};
  var changeImage = false;
  if(req.body.name){
    data.$set.name = req.body.name;
  }
  if(req.body.description){
    data.$set.description = req.body.description;
  }
  if(req.body.merchant){
    data.$set.merchant = req.body.merchant;
  }
  if(req.body.discount){
    data.$set.discount = req.body.discount;
  }
  if(req.body.newFileName){
    if(!req.body.image){
      return res.status(500).send({error: "La promoción debe contener una imagen asociada", code: 5});
    }
    data.$set.fileName = req.body.newFileName;
    changeImage = true;
  }
  var query = {};
  query._id = new mongo.ObjectID(req.params.promotion);
  db.collection('promotions').findAndModify(query, [['_id','asc']], data, {'new': 'true'}, function(err, promotion){
    if (err){
      console.log(err);
      return res.status(500).send({message: "Error al acceder a la base de datos", error: err});
    } 
    if(changeImage){
      var image = new Buffer(req.body.image.split('base64,')[1], "base64");
      aws.uploadImageToBucket(image, req.body.newFileName, function(awsRes){
        if(awsRes.success){
          promotion.value.id = promotion.value._id;
          promotion.value.url = config.PRODUCT_BASE_URL + promotion.value.fileName;
          return res.status(200).send({promotion: promotion.value})
        }
        else{
          return res.status(500).send({message: "Error al subir imagen", success: false, data: e.error});
        }
      });
    }
    else{
      promotion.value.id = promotion.value._id;
      promotion.value.url = config.PRODUCT_BASE_URL + promotion.value.fileName;
      return res.status(200).send({promotion: promotion.value})
    }
  });
};

exports.delete_promotion = function(req, res) {
  var query = {};
  query._id = new mongo.ObjectID(req.params.promotion);
  db.collection('promotions').findAndModify(query, [], {}, {remove: true, new: false}, function(err, promotion){
    if (err){
      console.log(err);
      return res.status(500).send({message: "Error al acceder a la base de datos", error: err});
    }
    return res.status(204).send();
  });
};