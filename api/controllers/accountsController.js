'use strict';
var config = require('../config');
const { Pool, Client } = require('pg');

exports.query_user_accounts = function(user, callback) {
  const client = new Client({
    connectionString: config.DATABASE_URL,
  })
  client.connect().then(function(){
    var query = client.query('SELECT balance, code, name, description, file as filename, alias, products.id as product_id, user_accounts.id as account_id ' +
      'FROM products, user_accounts ' +
      'WHERE user_accounts.product_id = products.id AND user_id = $1 ORDER BY product_id, code', [user]);
    query.then(function(q){
      var accounts = [];
      for(var i = 0; i < q.rowCount; i++){
        var alias = q.rows[i].code;
        if(q.rows[i].alias){
          alias = q.rows[i].alias;
        }
        accounts[i] = exports.mapAccount(q.rows[i]);
      }
      callback({status: 200, accounts: accounts, success: true});
      client.end();
    }).catch(function(e){
      console.log(e);
      callback({status: 500, message: "Error con la base de datos", success: false, data: e.error});
      client.end();
    });
  }).catch(function(e){
    console.log(e);
    callback({status: 500, message: "Error con la base de datos", success: false, data: e.error});
  });
};

exports.get_user_accounts = function(req, res) {
  exports.query_user_accounts(req.params.user, function(response){
    if(response.success){
      return res.status(response.status).send({accounts: response.accounts});
    }
    else{
      return res.status(response.status).send({message: response.message, success: false, data: response.data});
    }
  });
};

exports.modify_user_account = function(req, res) {
  const client = new Client({
    connectionString: config.DATABASE_URL,
  })
  client.connect().then(function(){
    var query = client.query('UPDATE user_accounts SET alias = $1 ' +
      'WHERE user_id = $2 AND id = $3', [req.body.alias, req.params.user, req.params.account]);
    query.then(function(u){
      if(u.rowCount == 1){
        res.status(204).send({});
      }
      else{
        res.status(500).send({message: "Error al actualizar la cuenta", success: false});
      }
      client.end();
    }).catch(function(e){
      console.log(e);
      res.status(500).send({message: "Error con la base de datos", success: false, data: e.error});
      client.end();
    });
  }).catch(function(e){
    console.log(e);
    res.status(500).send({message: "Error con la base de datos", success: false, data: e.error});
  });
};

exports.create_account = function(req, res) {
  const client = new Client({
    connectionString: config.DATABASE_URL,
  })
  client.connect().then(function(){
    var query = client.query('INSERT INTO user_accounts (user_id, product_id, balance, code, alias) ' +
                             'VALUES ($1, $2, $3, $4, $4) RETURNING id', 
                [req.body.user, req.body.product_id, req.body.balance, req.body.code]);
    query.then(function(u){
      if(u.rowCount == 1){
        req.body.account_id = u.rows[0].id;
        req.body.alias = req.body.code;
        req.body.filename = req.body.fileName;
        var account = exports.mapAccount(req.body);
        
        res.status(201).send({message: "Cuenta creada", account: account});
        client.end();
      }
    }).catch(function(e){
      console.log(e);
      res.status(500).send({message: "Error con la base de datos", success: false, data: e.error});
      client.end();
    });
  }).catch(function(e){
    console.log(e);
    res.status(500).send({message: "Error con la base de datos", success: false, data: e.error});
  });
};

exports.modify_account = function(req, res) {
  const client = new Client({
    connectionString: config.DATABASE_URL,
  })
  client.connect().then(function(){
    var query = client.query('UPDATE user_accounts SET product_id = $1, balance = $2, code = $3' +
      'WHERE id = $4', [req.body.product_id, req.body.balance, req.body.code, req.params.account]);
    query.then(function(u){
      if(u.rowCount == 1){
        req.body.account_id = req.params.account;
        req.body.filename = req.body.fileName;
        var account = exports.mapAccount(req.body);
        res.status(200).send({message: "Cuenta modificada!", account: account});
      }
      else{
        res.status(500).send({message: "Error al actualizar la cuenta", success: false});
      }
      client.end();
    }).catch(function(e){
      console.log(e);
      res.status(500).send({message: "Error con la base de datos", success: false, data: e.error});
      client.end();
    });
  }).catch(function(e){
    console.log(e);
    res.status(500).send({message: "Error con la base de datos", success: false, data: e.error});
  });
};

exports.delete_account = function(req, res) {
  const client = new Client({
    connectionString: config.DATABASE_URL,
  })
  client.connect().then(function(){
    var query = client.query('DELETE FROM user_accounts WHERE id = $1', [req.params.account]);
    query.then(function(q){
      if(q.rowCount != 1){
        res.status(500).send({message: "Error con la base de datos", success: false, data: "Error al borrar la cuenta"});
      }
      else{
        res.status(204).send();
      }
      client.end();
    }).catch(function(e){
      console.log(e);
      res.status(500).send({message: "Error con la base de datos", success: false, data: e.error});
      client.end();
    });
  }).catch(function(e){
    console.log(e);
    res.status(500).send({message: "Error con la base de datos", success: false, data: e.error});
  });
};

exports.mapAccount = function(row){
  if(row.account_id){
    var account = {id: row.account_id,
      product_id: row.product_id,
      code: row.code,
      alias: row.alias,
      balance: row.balance,
      product_name: row.product_name,
      description: row.description,
      fileName: row.filename,
      url: config.PRODUCT_BASE_URL + row.filename};

    return account;
  }
  return undefined;
};
