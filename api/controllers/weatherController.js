'use strict';
var config = require('../config');
const rj = require('request-json');

exports.get_weather = function(req, res) {
  var city = "Madrid,ES"
  if(req.query.city){
    city = req.query.city;
  }
  var query = "&q=" + city +"&units=metric&lang=es"
  var client = rj.createClient(config.WEATHER_URL + "?appid=" + config.WEATHER_API_KEY + query);
  client.get("", function(err, r, body){
    if(err){
      console.log(err);
      res.status(err.status).send(body);
    }
    else{
      var weather = {description: body.weather[0].description,
                     temperature: body.main.temp,
                     humidity: body.main.humidity,
                     city: body.name,
                     icon: config.WEATHER_ICON_BASE_URL + body.weather[0].icon + config.WEATHER_ICON_EXT}
      res.status(200).send({weather: weather});
    }
  });
};