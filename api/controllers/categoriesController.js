'use strict';
const { Pool, Client } = require('pg');
var config = require('../config');

exports.get_categories = function(req, res) {
  const client = new Client({
    connectionString: config.DATABASE_URL,
  })
  client.connect().then(function(){
    var filter = "";
    var parameters = [];
    if(req.params.category){
      filter = 'WHERE id = $1 ';
      parameters.push(req.params.category);
    }
    var query = client.query('SELECT id, name, description FROM categories ' + filter + ' ORDER BY name', parameters);
    query.then(function(q){
      var categories = [];
      for(var i = 0; i < q.rowCount; i++){
        categories[i] = {id: q.rows[i].id,
                       name: q.rows[i].name,
                       description: q.rows[i].description};
      }
      
      res.status(200).send({count: categories.length, categories: categories});
      client.end();
    }).catch(function(e){
      console.log(e);
      res.status(500).send({message: "Error con la base de datos", success: false, data: e.error});
      client.end();
    });
  }).catch(function(e){
    console.log(e);
    res.status(500).send({message: "Error con la base de datos", success: false, data: e.error});
  });
};

exports.create_category = function(req, res){
  const client = new Client({
    connectionString: config.DATABASE_URL,
  })
  client.connect().then(function(){
    var query = client.query('SELECT id FROM categories WHERE name = $1', [req.body.name]);

    query.then(function(q){
      if(q.rowCount > 0){
        res.status(500).send({message: "Categoría ya existente"});
        client.end();
      }
      else{
        query = client.query('INSERT INTO categories(name, description) VALUES ($1, $2) RETURNING id', 
          [req.body.name, req.body.description]);

        query.then(function(u){
          console.log(u);
          if(u.rowCount == 1){
            var category = {id: u.rows[0].id,
                            name: req.body.name,
                            description: req.body.description};
            
            res.status(201).send({message: "Categoría creada", category: category});
          }
          client.end();
        }).catch(function(e){
          console.log(e);
          res.status(500).send({message: "Error con la base de datos", success: false, data: e.error});
          client.end();
        });
      }
    }).catch(function(e){
      console.log(e);
      res.status(500).send({message: "Error con la base de datos", success: false, data: e.error});
      client.end();
    });
  }).catch(function(e){
    console.log(e);
    res.status(500).send({message: "Error con la base de datos", success: false, data: e.error});
  });
};

exports.modify_category = function(req, res){
  const client = new Client({
    connectionString: config.DATABASE_URL,
  })
  client.connect().then(function(){
    var i = 1;
    var fields = "";
    var params = [];
    if(req.body.name){
      fields = "name = $" + i++;
      params.push(req.body.name);
    }
    if(req.body.description){
      if(i > 1){
        fields += ", ";
      }
      fields += "description = $" + i++;
      params.push(req.body.description);
    }
    
    if(params.length == 0){
      return res.status(500).send({message: "No se han indicado campos!", success: false, });  
    }
    params.push(req.params.category);
    var query = client.query('UPDATE categories SET ' + fields + ' WHERE id = $' + i, params);
    query.then(function(q){
      if(q.rowCount != 1){
        return res.status(500).send({message: "Error con la base de datos", success: false, data: "Error al actualizar la categoria"});
      }

      var category = {id: req.params.category,
        name: req.body.name,
        description: req.body.description};

      res.status(200).send({message: "Categoría modificada", category: category});
      client.end();
    }).catch(function(e){
      console.log(e);
      res.status(500).send({message: "Error con la base de datos", success: false, data: e.error});
      client.end();
    });
  }).catch(function(e){
    console.log(e);
    res.status(500).send({message: "Error con la base de datos", success: false, data: e.error});
  });  
};

exports.delete_category = function(req, res){
  const client = new Client({
    connectionString: config.DATABASE_URL,
  })
  client.connect().then(function(){
    var query = client.query('DELETE FROM categories WHERE id = $1', [req.params.category]);
    query.then(function(q){
      if(q.rowCount != 1){
        res.status(500).send({message: "Error con la base de datos", success: false, data: "Error al borrar la categoria"});
      }
      else{
        return res.status(200).send();
      }
      client.end();      
    }).catch(function(e){
      console.log(e);
      res.status(500).send({message: "Error con la base de datos", success: false, data: e.error});
      client.end();
    })
  }).catch(function(e){
    console.log(e);
    res.status(500).send({message: "Error con la base de datos", success: false, data: e.error});
  });
};