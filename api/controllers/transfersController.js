'use strict';
var config = require('../config');
const { Pool, Client } = require('pg');
const rj = require('request-json');

exports.create_transfer = function(req, res) {
  const client = new Client({
    connectionString: config.DATABASE_URL,
  })
  client.connect().then(function(){
    var query = client.query('SELECT balance FROM user_accounts WHERE user_id = $1 AND id = $2', [req.params.user, req.params.account]);
    query.then(function(q){
      if(q.rowCount != 1){
        client.end();
        return res.status(500).send({message: "Error con la base de datos", success: false, data: "Error al recuperar los datos de origen"});
      }
      if(q.rows[0].balance < req.body.quantity){
        client.end();
        return res.status(500).send({message: "No dispone del saldo suficiente en la cuenta", success: false});
      }
      var fromBalance = new Number(q.rows[0].balance);
      query = client.query('SELECT id, balance, user_id FROM user_accounts WHERE code = $1', [req.body.to]);

      query.then(function(q){
        if(q.rowCount != 1){
          client.end();
          return res.status(500).send({message: "Error con la base de datos", success: false, data: "Error al recuperar los datos de destino"});
        }
        var toBalance = new Number(q.rows[0].balance);
        var toId = q.rows[0].id;
        var toUser = q.rows[0].user_id;
        if(toId == req.params.account){
          client.end();
          return res.status(500).send({message: "La cuenta origen y la cuenta destino deben ser diferentes", success: false});
        }
        
        var update = 'UPDATE user_accounts SET balance = $1 WHERE id = $2';
        query = client.query(update, [fromBalance - req.body.quantity, req.params.account]);
        query.then(function(q){
          if(q.rowCount != 1){
            client.end();
            return res.status(500).send({message: "Error con la base de datos", success: false, data: "Error al actualizar origen"});
          }
          query = client.query(update, [toBalance + req.body.quantity, toId]);
          query.then(function(q){
            if(q.rowCount != 1){
              client.end();
              return res.status(500).send({message: "Error con la base de datos", success: false, data: "Error al actualizar destino"});
            }
            client.end();
            createMovements(req, res, toId, toUser);
          }).catch(function(e){
            console.log(e);
            client.end();
            return res.status(500).send({message: "Error con la base de datos", success: false, data: e.error});
          });
        }).catch(function(e){
          console.log(e);
          client.end();
          return res.status(500).send({message: "Error con la base de datos", success: false, data: e.error});
        });
      }).catch(function(e){
        console.log(e);
        client.end();
        return res.status(500).send({message: "Error con la base de datos", success: false, data: e.error});
      });
    }).catch(function(e){
      console.log(e);
      client.end();
      return res.status(500).send({message: "Error con la base de datos", success: false, data: e.error});
    });
  }).catch(function(e){
    client.end();
    console.log(e);
    return res.status(500).send({message: "Error con la base de datos", success: false, data: e.error});
  });
};

var createMovements = function(req, res, toId, toUser){
  var date = new Date();
  var dataFrom = {
    user: req.params.user,
    date: {$date: date.toISOString()},
    type: 2,
    concept: "Transferencia emitida",
    detail: req.body.concept,
    category: 5,
    quantity: new Number(req.body.quantity),
    account: req.params.account,
  };
  var dataTo = {
    user: toUser + "",
    date: {$date: date.toISOString()},
    type: 0,
    concept: "Transferencia recibida",
    detail: req.body.concept,
    category: 5,
    quantity: new Number(req.body.quantity),
    account: toId + "",
  };
  var client = rj.createClient(config.MLAB_URL + "?" + config.MLAB_API_KEY);
  client.post("", [dataFrom, dataTo], function(err, r, body){
    if(err){
      res.status(err.status).send(body);
    }
    else{
      res.status(201).send(body);
    }
  });
}