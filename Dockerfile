# Imagen base
FROM node:slim

#Directorio de trabajo
WORKDIR /app

#Copiar archivos
ADD package.json /app
ADD server.js /app
ADD api /app/api
ADD keys /app/keys


#Instalar dependencias
RUN npm install

#Puerto
EXPOSE 7000 7443
ENV MONGO_PWD=####
ENV PG_PWD=####
ENV TOKEN_SECRET=####

#Command
CMD ["npm", "start"]
